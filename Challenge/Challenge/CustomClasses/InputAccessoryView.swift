//
//  InputAccessoryView.swift
//
//  Created by Rodolfo Antonici on 23/08/16.
//  Copyright © 2016 RAntonici. All rights reserved.
//

import UIKit

class InputAccessoryView: UIView {

    private var toolbar = UIToolbar()
    private var rightBarButton: UIBarButtonItem!
    weak var textField: UITextField? {
        didSet{
            guard let textField = textField else{
                return
            }
            updateRightBarButtonTitleFor(textField: textField)
        }
    }
    
    override var frame: CGRect {
        didSet {
            self.updateLayerFrames()
        }
    }
    
    func updateRightBarButtonTitleFor(textField: UITextField) {
        var title = ""
        switch textField.returnKeyType {
        case .default:
        title = "Done"
        case .go:
        title = "Go"
        case .google:
        title = "Search"
        case .join:
        title = "Join"
        case .next:
        title = "Next"
        case .route:
        title = "Route"
        case .search:
        title = "Search"
        case .send:
        title = "Send"
        case .yahoo:
        title = "Search"
        case .done:
        title = "Done"
        case .emergencyCall:
        title = "Emergency Call"
        case .continue:
        title = "Done"
        }
        self.rightBarButton.title = title
        
    }
    
    convenience init(frame: CGRect, withTextField textField: UITextField) {
        self.init(frame: frame)
        updateRightBarButtonTitleFor(textField: textField)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.toolbar.barTintColor = UIColor(red: 0/255, green: 12/255, blue: 17/255, alpha: 1)
        self.toolbar.tintColor = .white
        self.addSubview(self.toolbar)
        self.rightBarButton = UIBarButtonItem(title: "Return", style: .plain, target: self, action: #selector(rightButtonDidReceiveTouchUpInside))
        let flexSpacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        self.toolbar.items = [flexSpacer, self.rightBarButton]
        self.updateLayerFrames()
    }
    
    @objc func rightButtonDidReceiveTouchUpInside() {
        if let delegate = self.textField?.delegate {
            let _ = delegate.textFieldShouldReturn!(self.textField!)
        }
        
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
    }
    
    func updateLayerFrames() {
        self.toolbar.frame = CGRect(origin: CGPoint.zero, size: self.frame.size)
    }

}
