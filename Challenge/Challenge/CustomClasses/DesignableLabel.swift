//
//  DesignableLabel.swift
//  Challenge
//
//  Created by Rodolfo Antonici on 23/04/18.
//  Copyright © 2018 Rodolfo Antonici. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableLabel: UILabel {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
}
