//
//  ZupColorsExtension.swift
//  Challenge
//
//  Created by Rodolfo Antonici on 23/04/18.
//  Copyright © 2018 Rodolfo Antonici. All rights reserved.
//

import UIKit
class ZupColor {
    open class var darkBlue: UIColor {
        return UIColor(red: 0/255.0, green: 50/255.0, blue: 78/255.0, alpha: 1)
    }
    open class var darkGreen: UIColor {
        return UIColor(red: 7/255.0, green: 168/255.0, blue: 0/255.0, alpha: 1)
    }
    open class var lemonGreen: UIColor {
        return UIColor(red: 142/255.0, green: 182/255.0, blue: 0/255.0, alpha: 1)
    }
    open class var orange: UIColor {
        return UIColor(red: 253/255.0, green: 85/255.0, blue: 28/255.0, alpha: 1)
    }
}
