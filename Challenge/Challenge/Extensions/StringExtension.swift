//
//  StringExtension.swift
//  Challenge
//
//  Created by Rodolfo Antonici on 11/04/18.
//  Copyright © 2018 Rodolfo Antonici. All rights reserved.
//

import UIKit
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [.font: font],
                                            context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func base64EncodedString() -> String? {
        return self.data(using: .utf8)?.base64EncodedString()
    }
}
