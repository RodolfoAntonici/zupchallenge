//
//  UIFontExtension.swift
//  Challenge
//
//  Created by Rodolfo Antonici on 25/04/18.
//  Copyright © 2018 Rodolfo Antonici. All rights reserved.
//

import UIKit

extension UIFont {
    open class func sansationFont(ofSize fontSize: CGFloat) -> UIFont {
        
        return UIFont(name: "Sansation", size: fontSize)!
    }
}
