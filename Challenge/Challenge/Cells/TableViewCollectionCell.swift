//
//  TableViewCollectionCell.swift
//  Challenge
//
//  Created by Rodolfo Antonici on 11/04/18.
//  Copyright © 2018 Rodolfo Antonici. All rights reserved.
//

import UIKit
fileprivate let MessageCellIdentifier = "Message"
fileprivate let AnswerCellIdentifier = "Answer"
fileprivate let EmptyCellIdentifier = "Empty"
fileprivate let QuestionHeaderIdentifier = "QuestionHeader"

protocol TableViewCollectionViewCellDelegate: class {
    func tableViewCollectionViewCell(_ cell: TableViewCollectionViewCell, didSelectAtRow row: Int)
}

class TableViewCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var tableView: UITableView?
    weak var delegate: TableViewCollectionViewCellDelegate?
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var tableData: ChallengeViewController.SectionData? {
        didSet{
            tableView?.reloadData()
//            tableViewHeightConstraint.constant = tableView?.contentSize.height ?? frame.size.height
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView?.register(UINib(nibName: "QuestionTableViewHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: QuestionHeaderIdentifier)
        tableView?.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView?.estimatedSectionHeaderHeight = 110
        tableView?.rowHeight = UITableViewAutomaticDimension
        tableView?.estimatedRowHeight = 60
//        tableView?.isScrollEnabled = false
        tableView?.dataSource = self
        tableView?.delegate = self
    }
}

extension TableViewCollectionViewCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData?.cellsData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AnswerCellIdentifier, for: indexPath) as! AnswerTableViewCell
        
        guard let cellData = tableData?.cellsData?[indexPath.row] else { return cell }
        cell.alternativeLabel?.text = cellData.index
        cell.answerLabel?.text = cellData.answer
        cell.isRightAnswer = tableData?.correctAnswerRow == indexPath.row
        return cell
    }
}


extension TableViewCollectionViewCell: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        guard tableView.indexPathForSelectedRow == nil else { return nil }
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.tableViewCollectionViewCell(self, didSelectAtRow: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: QuestionHeaderIdentifier) as! QuestionTableViewHeaderView
        sectionHeaderView.numberLabel?.text = tableData?.number
        sectionHeaderView.titleLabel?.text = tableData?.title
        return sectionHeaderView
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return tableData?.height ?? 45
//    }
}
