//
//  AnswerTableViewCell.swift
//  Challenge
//
//  Created by Rodolfo Antonici on 09/04/18.
//  Copyright © 2018 Rodolfo Antonici. All rights reserved.
//

import UIKit

extension AnswerTableViewCell {
    struct Model{
        var alternative: String?
        var answer: String?
    }
}

class AnswerTableViewCell: UITableViewCell {
    // 70% on the countour and 10% on the background

    @IBOutlet weak var alternativeLabel: DesignableLabel!
    @IBOutlet weak var answerLabel: UILabel!
    var isRightAnswer = false
    
    var presentationColor: UIColor = .black {
        didSet{
            didSetPresentationColor()
        }
    }
    
    @IBOutlet weak var cuteDesignView: DesignableView! // I had no better idea on the name, ok? If you want to, rename it
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        guard selected else {
            presentationColor = ZupColor.lemonGreen
            return
        }

        presentationColor = isRightAnswer ? ZupColor.lemonGreen : ZupColor.orange
    }
    
    
    private func didSetPresentationColor() {
        cuteDesignView.borderColor = presentationColor
        alternativeLabel.backgroundColor = presentationColor
        
        if isSelected {
            cuteDesignView.backgroundColor = presentationColor
            answerLabel.textColor = ZupColor.darkBlue
        }
        else {
            cuteDesignView.backgroundColor = .clear
            answerLabel.textColor = presentationColor
        }
        
    }
}
