//
//  QuestionTableViewHeader.swift
//  Challenge
//
//  Created by Rodolfo Antonici on 09/04/18.
//  Copyright © 2018 Rodolfo Antonici. All rights reserved.
//

import UIKit

class QuestionTableViewHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
}
