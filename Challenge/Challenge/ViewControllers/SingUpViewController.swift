//
//  SingUpViewController.swift
//  Challenge
//
//  Created by Rodolfo Antonici on 12/04/18.
//  Copyright © 2018 Rodolfo Antonici. All rights reserved.
//

import UIKit
import FirebaseDatabase
import InputMask

fileprivate let ChallengeSegueIdentifier = "Challenge"

/// This make it ignores the firebase verification and calls the fakeData() upon ViewDidAppear,
//  The tester only needs to select wich question group it will apply
fileprivate let TestMode = false



class SingUpTableViewController: UITableViewController, MaskedTextFieldDelegateListener {
//    var firebaseDatabase = Database.database().reference()
    var maskedDelegate: MaskedTextFieldDelegate!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var challangerLabel: UILabel!
    @IBOutlet weak var qrScanIconImageView: UIImageView!
    
    @IBOutlet weak var skillSegmentedControl: UISegmentedControl!
    
    lazy var footerView: UIView = self.configureFooterView()
    weak var startButton: UIButton?
    
    
    var user: User? { didSet{ didSetUser() } }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextField()
        configureTableView()
        startListenKeyboardNotifications()
        
        challangerLabel.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if TestMode {
            fakeData()
        }
    }
    
    func fakeData() {
        user = User()
        user?.name = "Zupper"
        user?.organization = "Wololo"
        user?.mail = "rodolfo@hotmail.com"
        user?.title = "Title"
        user?.phone = "34999999999"
        
    }
    
//    var name: String?
//    var organization: String?
//    var mail: String?
//    var title: String?
//    var phone: String?
//    var skill: Skill?
    
    fileprivate func configureTextField() {
        let accessoryView = InputAccessoryView(frame: CGRect(x: 0, y: 0,
                                                             width: tableView.bounds.size.width, height: 44),
                                               withTextField: phoneTextField)
        phoneTextField.inputAccessoryView = accessoryView
        
        
        accessoryView.textField = phoneTextField
        
        phoneTextField.attributedPlaceholder = phoneTextFieldPlaceholderAttributedString()

        maskedDelegate = MaskedTextFieldDelegate(format: "{+55} ([00]) [90000]-[0000]")
        maskedDelegate.listener = self
        phoneTextField.delegate = maskedDelegate

    }
    
    private func phoneTextFieldPlaceholderAttributedString() -> NSAttributedString {
        let placeholderColor = UIColor.white.withAlphaComponent(0.7)
        let boldAttributes: [NSAttributedStringKey : Any] = [.font : UIFont.systemFont(ofSize: 18, weight: .black),
                                                             .foregroundColor : placeholderColor]
        let regularAttributes: [NSAttributedStringKey : Any] = [.font : UIFont.sansationFont(ofSize: 18),
                                                                .foregroundColor : placeholderColor]
        
        let attributedPlaceholder = NSMutableAttributedString(string: "+55 (",
                                                              attributes: regularAttributes)
        
        attributedPlaceholder.append(NSAttributedString(string: "••",
                                                        attributes: boldAttributes))
        attributedPlaceholder.append(NSAttributedString(string: ")",
                                                        attributes: regularAttributes))
        attributedPlaceholder.append(NSAttributedString(string: "•••••",
                                                        attributes: boldAttributes))
        attributedPlaceholder.append(NSAttributedString(string: "-",
                                                        attributes: regularAttributes))
        attributedPlaceholder.append(NSAttributedString(string: "••••",
                                                        attributes: boldAttributes))
        
        return attributedPlaceholder
        
    }
    
    fileprivate func configureTableView() {
        tableView.backgroundView = UIView(frame: tableView.bounds)
        let backgroundImageView = UIImageView(image: UIImage(named: "background"))
        backgroundImageView.frame = tableView.bounds
        tableView.backgroundView?.addSubview(backgroundImageView)
    }
    
    fileprivate func startListenKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow),
                                               name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: .UIKeyboardWillHide, object: nil)
    }
    
    
    @objc func keyboardWillShow() {
        self.footerView.isHidden = true
    }
    
    @objc func keyboardWillHide() {
        self.footerView.isHidden = false
    }
    
    func configureFooterView() -> UIView {
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 68))
        let startButton = DesignableButton(type: .custom)
        
        startButton.setTitle("Start!", for: .normal)
        startButton.frame = CGRect(x: 20, y: 4, width: view.frame.size.width-40, height: 44)
        startButton.addTarget(self, action: #selector(startButtonTouchUpInside), for: .touchUpInside)
        startButton.cornerRadius = 5
        startButton.setTitleColor(.white, for: .normal)
        startButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        startButton.backgroundColor = ZupColor.lemonGreen
        footerView.addSubview(startButton)
        
        self.startButton = startButton
        
        return footerView
    }
    
    func updateStartButtonStatus() {
        guard let user = user, let phone = user.phone, !phone.isEmpty else {
            startButton?.isEnabled = false
            return
        }
        startButton?.isEnabled = true
    }
    
    @objc func startButtonTouchUpInside() {
        let rootRef = Database.database().reference()
        guard let user = user, let uid = user.uid else {
            let alert = UIAlertController(title: ":(", message: "Ops, didn't you forget to scan the QRCode?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes!", style: .default, handler: { (action) in
                self.openQRCodeScannerViewController()
            }))
            present(alert, animated: true, completion: nil)
            
            return
        }
        
        
        user.phone = phoneTextField.text
        if user.phone == nil || user.phone!.isEmpty {
            let alert = UIAlertController(title: "Hey \(user.name!)!", message: "We need your number so we can contact you when you win", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok!", style: .default, handler: { (action) in
                self.phoneTextField.becomeFirstResponder()
            }))
            
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        
        user.skill = skillAtSelectedSegmentIndex(skillSegmentedControl.selectedSegmentIndex)
        if user.skill == nil {
            let alert = UIAlertController(title: "Hey \(user.name!)!", message: "You need to select which skill you will test!", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok!", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            return
        }

        
        if TestMode {
            startChallange()
            return
        }
        
        rootRef.child("challenges").queryEqual(toValue: nil, childKey: uid).observeSingleEvent(of: .value) { (snapshot) in
            print("Deu")
            if snapshot.childrenCount == 0 {
                self.startChallange()
            }
            else {
                let alert = UIAlertController(title: "Hey \(user.name!)!", message: "You already did the challenge, you can't do it again", preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "Ok :'( ", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func skillAtSelectedSegmentIndex(_ index: Int) -> Skill? {
        switch index {
        case 0:
            return .android
        case 1:
            return .backend
        case 2:
            return .frontend
        case 3:
            return .iOS
        default:
            break
        }
        return nil
    }
    
    func startChallange() {
        
        guard let user = user else {
            let alert = UIAlertController(title: ":(", message: "Ops, didn't you forget to scan the QRCode?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes!", style: .default, handler: { (action) in
                self.openQRCodeScannerViewController()
            }))
            alert.addAction(UIAlertAction(title: "Nop!", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        
        performSegue(withIdentifier: ChallengeSegueIdentifier, sender: user)

    }

    
    @IBAction func didChangeSkill(_ sender: UISegmentedControl) {
        user?.skill = skillAtSelectedSegmentIndex(sender.selectedSegmentIndex)
    }
    
    func didSetUser() {
        
        guard let user = user,
            let nameSubstring = user.name?.split(separator: " ").first else {
                qrScanIconImageView.isHidden = false
                challangerLabel.isHidden = true
                return
        }
        
        challangerLabel.isHidden = false
        qrScanIconImageView.isHidden = true
        let firstName = String(nameSubstring)
        
        let attributedString = NSMutableAttributedString(string: "Hello \(firstName)!",
                                                        attributes: [.font : UIFont.sansationFont(ofSize: 30),
                                                                     .foregroundColor : ZupColor.lemonGreen])
        
        attributedString.append(NSAttributedString(string: "\n\nNow we just one more thing\nyour phone number so we can\ncall you when you win the iPhoneX!",
                                                   attributes: [.font : UIFont.sansationFont(ofSize: 18),
                                                                .foregroundColor : UIColor.white]))

        challangerLabel.attributedText = attributedString
        
        user.phone = phoneTextField.text
        user.skill = skillAtSelectedSegmentIndex(skillSegmentedControl.selectedSegmentIndex)
    }
    
    func openQRCodeScannerViewController() {
        let qrCodeScanner = storyboard?.instantiateViewController(withIdentifier: "QRScanner") as! QRScannerController
        qrCodeScanner.delegate = self
        navigationController?.present(qrCodeScanner, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier ?? "" {
        case ChallengeSegueIdentifier:
            let challengeViewController = segue.destination as! ChallengeViewController
            challengeViewController.user = user
        default:
            break
        }
    }
    
    
    @IBAction func unwindFromResultViewController(segue:UIStoryboardSegue) {
        user = nil
        phoneTextField.text = nil
        skillSegmentedControl.selectedSegmentIndex = -1
    }
}


extension SingUpTableViewController /*TableViewDelegate*/{
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            openQRCodeScannerViewController()
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return footerView.frame.size.height
    }
}


extension SingUpTableViewController: QRScannerControllerDelegate {
    func scannerController(_ scannerController: QRScannerController, didScanUser user: User?) {
        guard let user = user else {
            let alert = UIAlertController(title: ":(", message: "Ops, The QRCode couldn't be read, try again", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        
        self.user = user
    }
    
    func scannerControllerDidFail(_ scannerController: QRScannerController) {
        
    }
    
    
}

extension SingUpTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
}
