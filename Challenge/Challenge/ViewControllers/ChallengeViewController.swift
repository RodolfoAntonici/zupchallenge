//
//  ChallengeViewController.swift
//  Challenge
//
//  Created by Rodolfo Antonici on 06/04/18.
//  Copyright © 2018 Rodolfo Antonici. All rights reserved.
//

import UIKit


fileprivate let TableCellIdentifier = "Table"
fileprivate let ResultSegueIdentifier = "Result"
fileprivate let ChallengeTime: TimeInterval = 301


extension ChallengeViewController {
    struct SectionData {
        var title: String?
        var number: String?
        var cellsData: [CellData]?
        var correctAnswerRow: Int?
        var height: CGFloat?
    }
    
    struct CellData {
        var answer: String?
        var index: String?
    }
}



class ChallengeViewController: UIViewController {
    @IBOutlet weak var pageIndicator: UIPageControl!
    
    fileprivate var correctAnswersCount = 0
    @IBOutlet weak var countDownLabel: UILabel!
    var user: User?
    var collectionData: [SectionData]?
    @IBOutlet weak var collectionView: UICollectionView?
    let feedbackGenerator = UINotificationFeedbackGenerator()
    var countDownTimer: Timer?
    var countDownTime = ChallengeTime
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.isScrollEnabled = false
        loadData()
        startTimer()
        feedbackGenerator.prepare()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.countDownTimer?.invalidate()
    }
    
    func startTimer() {
        
        countDownTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            if(self.countDownTime > 0) {
                self.countDownTime -= 1
                self.countDownLabel.text = self.timeString(time: self.countDownTime)
                if self.countDownTime < 60 {
                    self.countDownLabel.textColor = ZupColor.orange
                }
            }
            else {
                self.countDownTimer?.invalidate()
                self.presentResultViewController()
            }
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        let minutes = Int(time) / 60
        let seconds = time.truncatingRemainder(dividingBy: 60)
        return String(format:"%02i:%02i", minutes, Int(seconds))
    }
    
    func pathForUserSkill() -> String {
        let resource: String
        switch user!.skill! {
        case .android:
            resource = "AndroidQuestions"
        case .backend:
            resource = "BackendQuestions"
        case .frontend:
            resource = "FrontQuestions"
        case .iOS:
            resource = "iOSQuestions"
        }
        
        return Bundle.main.path(forResource: resource, ofType: "plist")!
    }
    
    func loadData() {
        
        let plistXML = FileManager.default.contents(atPath: pathForUserSkill())!
        
        let computingPlistPath: String = Bundle.main.path(forResource: "ComputingQuestions", ofType: "plist")! //the path of the data
        let computingPlistXML = FileManager.default.contents(atPath: computingPlistPath)!

        
        let devopsPlistPath: String = Bundle.main.path(forResource: "DevopsQuestion", ofType: "plist")! //the path of the data
        let devopsPlistXML = FileManager.default.contents(atPath: devopsPlistPath)!
        
        do {//convert the data to a dictionary and handle errors.
            
            let decoder = PropertyListDecoder()
            var computingQuestions = try decoder.decode([MutipleAnswerQuestion].self, from: computingPlistXML)
            computingQuestions.shuffle()
            computingQuestions = Array(computingQuestions.prefix(5))
            
            var devopsQuestions = try decoder.decode([MutipleAnswerQuestion].self, from: devopsPlistXML)
            devopsQuestions.shuffle()
            devopsQuestions = Array(devopsQuestions.prefix(5))
            
            var multipleQuestions = try decoder.decode([MutipleAnswerQuestion].self, from: plistXML)
            multipleQuestions.shuffle()
            multipleQuestions = Array(multipleQuestions.prefix(10))
            
            multipleQuestions += computingQuestions
            multipleQuestions += devopsQuestions
            
            multipleQuestions.shuffle()
            
            collectionData = [SectionData]()
            var questionCount = 0
            for question in multipleQuestions {
                guard let answers = question.answers else { return }
                var asciiValue = 65 //Equals to A
                
                var cellsData = [CellData]()
                for answer in answers {
                    cellsData.append(CellData(answer: answer,
                                              index: String(UnicodeScalar(UInt8(asciiValue)))))
                    asciiValue += 1
                }
                
                
                let textHeight = question.text?.height(withConstrainedWidth: view.frame.size.width - 45,
                                                       font: UIFont.systemFont(ofSize: 20))
                
                let currentSectionData = SectionData(title: question.text,
                                                     number: "\(questionCount)",
                                                     cellsData: cellsData,
                                                     correctAnswerRow: question.correctAnswer,
                                                     height: textHeight! + 28)
                questionCount += 1
                collectionData?.append(currentSectionData)
            }
            
            collectionView?.reloadData()

        } catch {
            print("Error reading plist: \(error))")
        }
       
    }
    
    func presentResultViewController() {
        self.performSegue(withIdentifier: ResultSegueIdentifier,
                          sender: Challenge(score: self.correctAnswersCount,
                                            time: ChallengeTime - countDownTime,
                                            user: self.user))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier ?? "" {
        case ResultSegueIdentifier:
            let resultViewController = segue.destination as! ResultViewController
            resultViewController.data = sender as? Challenge
            
        default:
            break
        }
    }
}


extension ChallengeViewController: UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TableCellIdentifier, for: indexPath) as! TableViewCollectionViewCell
        cell.delegate = self
        cell.tableData = collectionData?[indexPath.item]
        return cell
    }
}

extension ChallengeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
}

extension ChallengeViewController: TableViewCollectionViewCellDelegate {
    func tableViewCollectionViewCell(_ cell: TableViewCollectionViewCell, didSelectAtRow row: Int) {
        
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        pageIndicator.currentPage += 1
        if collectionData?[indexPath.item].correctAnswerRow == row {
            feedbackGenerator.notificationOccurred(.success)
            correctAnswersCount += 1
        }
        else {
            feedbackGenerator.notificationOccurred(.error)
        }
        

        if indexPath.item == collectionData!.count - 1 {
            self.presentResultViewController()
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) { 
            self.collectionView?.scrollToItem(at: IndexPath(item: indexPath.item + 1, section: indexPath.section),
                                         at: .left,
                                         animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.feedbackGenerator.prepare()
            }
            
        }
        
    }
    
    
}

