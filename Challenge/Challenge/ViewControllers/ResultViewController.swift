//
//  ResultViewController.swift
//  Challenge
//
//  Created by Rodolfo Antonici on 16/04/18.
//  Copyright © 2018 Rodolfo Antonici. All rights reserved.
//

import UIKit

import FirebaseDatabase

fileprivate let UnwindFromResultViewControllerSegueIdentifier = "UnwindToSignUp"

class ResultViewController: UIViewController {
    @IBOutlet weak var congratulationsLabel: UILabel!
    @IBOutlet weak var pontuationLabel: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var data: Challenge? {
        didSet {
            didSetData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        didSetData()
    }
    
    func didSetData() {
        write()
        guard let data = data, let nameArray = data.user?.name?.split(separator: " "),
            let firstName = nameArray.first, isViewLoaded  else { return }
        
        var name = String(describing: firstName)
        
        if let lastName = nameArray.last {
            name += " " + String(describing: lastName)
        }
        congratulationsLabel.text = "Parabéns\n\(name)"
        
        var pontuationLabelText = "Você fez \(data.score!) ponto"

        if data.score! > 1 {
            pontuationLabelText += "s"
        }
        
        pontuationLabel.text = pontuationLabelText
    }
    
    func write() {
        guard let data = data, let uid = data.user?.uid,
            let firebaseObject = try? FirebaseEncoder().encode(data) else { return }
        let rootRef = Database.database().reference()
        rootRef.child("challenges").child(uid).setValue(firebaseObject)
    }
    
    @IBAction func newChallengerButtonTouchUpInside() {
        performSegue(withIdentifier: UnwindFromResultViewControllerSegueIdentifier, sender: self)
    }
    
}
