//
//  User.swift
//  Challenge
//
//  Created by Rodolfo Antonici on 18/04/18.
//  Copyright © 2018 Rodolfo Antonici. All rights reserved.
//

import Foundation

enum Skill: String, Codable {
    case android = "Android"
    case backend = "Backend"
    case frontend = "Frontend"
    case iOS = "iOS"
}

class User: Codable {
    var name: String?
    var organization: String?
    var mail: String?
    var title: String?
    var phone: String?
    var skill: Skill?
    
    var uid: String? {
        guard let name = name , let mail = mail else { return nil }
        return (name + mail).base64EncodedString()
    }
    
    convenience init(withQRCodeString qrCodeString: String) {
        self.init()
        
        for substring in qrCodeString.split(separator: "\n") {
            let keyValue = substring.split(separator: ":")
            debugPrint(substring)
            if let keySubstring = keyValue.first, let valueSubstring = keyValue.last {
                debugPrint(keyValue)
                switch String(keySubstring) {
                case "FN":
                    self.name = String(valueSubstring)
                    
                case "EMAIL":
                    self.mail = String(valueSubstring)
                    
                case "TITLE":
                    self.title = String(valueSubstring)
                    
                case "ORG":
                    self.organization = String(valueSubstring)
                    
                default:
                    break
                }
            }
        }
    }
}
