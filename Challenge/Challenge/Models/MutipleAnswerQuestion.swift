//
//  MutipleAnswerQuestion.swift
//  Challenge
//
//  Created by Rodolfo Antonici on 09/04/18.
//  Copyright © 2018 Rodolfo Antonici. All rights reserved.
//

import Foundation

class MutipleAnswerQuestion: Codable {
    var text: String?
    var correctAnswer: Int?
    var answers: [String]?
    
}
