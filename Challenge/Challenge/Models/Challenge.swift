//
//  Challenge.swift
//  Challenge
//
//  Created by Rodolfo Antonici on 18/04/18.
//  Copyright © 2018 Rodolfo Antonici. All rights reserved.
//

import Foundation

struct Challenge: Codable {
    var score: Int?
    var time: Double?
    var user: User?
}
